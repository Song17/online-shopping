const initState = {
  goods: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case "SET_GOODS":
      return {
        ...state,
        goods: action.goods
      };
    default:
      return state
  }
};