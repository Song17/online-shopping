import {combineReducers} from "redux";
import goodsReducer from './goodsReuducer';
import orderReducer from "./orderReducer";

const reducers = combineReducers({
  goods: goodsReducer,
  order: orderReducer
});
export default reducers;