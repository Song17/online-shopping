const initState = {
  orders: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case "SET_ORDERS":
      return {
        ...state,
        orders: action.orders
      };
    default:
      return state
  }
};