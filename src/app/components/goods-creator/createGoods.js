const createGoods = goods => {
  fetch('http://localhost:8080/api/goods',
    {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(goods)
    }).then(response => response)
    .catch(error => console.log(error));
};

export default createGoods;
