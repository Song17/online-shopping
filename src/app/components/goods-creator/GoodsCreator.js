import React, {Component} from 'react';
import createGoods from './createGoods';

class GoodsCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      price: '',
      unit: '',
      image: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const goods = {name: this.name.value, price: this.price.value, unit: this.unit.value, image: this.image.value};
    createGoods(goods);
    this.props.history.push("/");
  }

  render() {
    return (
      <div className='goods-creator'>
        <h1>添加商品</h1>
        <form>
          <ul>
            <li>名称
              <input placeholder="名称" ref={input => this.name = input}/>
            </li>
            <li>价格
              <input placeholder="价格" ref={input => this.price = input}/>
            </li>
            <li>单位
              <input placeholder="单位" ref={input => this.unit = input}/>
            </li>
            <li>图片
              <input placeholder="URL" ref={input => this.image = input}/>
            </li>
          </ul>
          <button type='submit' onClick={this.handleClick}>提交</button>
        </form>
      </div>
    );
  }
}

export default GoodsCreator;