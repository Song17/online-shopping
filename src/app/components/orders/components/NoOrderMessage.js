import React from 'react';
import {Link} from "react-router-dom";

const NoOrderMessage = () => {
  return (
    <p>暂无订单，返回<Link to="/online-shopping">商城页面</Link>继续购买</p>
  );
};

export default NoOrderMessage;