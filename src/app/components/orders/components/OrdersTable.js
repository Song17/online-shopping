import React, {Component} from 'react';
import Order from "./Order";

class OrdersTable extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <table className="orders-table">
        <thead>
        <tr>
          <th>名字</th>
          <th>单价</th>
          <th>数量</th>
          <th>单位</th>
          <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {this.props.orders.map((order, key) => (<Order key={key} order={order}/>))}
        </tbody>
      </table>

    );
  }
}

export default OrdersTable;