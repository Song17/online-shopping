import React, {Component} from 'react';

class Order extends Component {

  constructor(props) {
    super(props);
    this.deleteOrderByUserAndGoodsId = this.deleteOrderByUserAndGoodsId.bind(this);
  }

  deleteOrderByUserAndGoodsId() {
    const {user, goodsId} = this.props.order;
    fetch(`http://localhost:8080/api/orders?user=${user}&goodsId=${goodsId}`,
      {
        method: 'DELETE'
      }).then(response => window.location.href = "/online-shopping/orders")
      .catch(error => alert("订单删除失败，请稍后再试"));
  }

  render() {
    const {name, price, unit, quantity} = this.props.order;
    return (
      <tr className="order">
        <td>{name}</td>
        <td>{price}</td>
        <td>{quantity}</td>
        <td>{unit}</td>
        <td>
          <button onClick={this.deleteOrderByUserAndGoodsId}>删除</button>
        </td>
      </tr>
    );
  }
}

export default Order;