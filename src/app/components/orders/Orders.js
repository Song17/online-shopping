import React, {Component} from 'react';
import {connect} from "react-redux";
import {setOrders} from "../../actions/orderAction";
import {bindActionCreators} from "redux";
import NoOrderMessage from "./components/NoOrderMessage";
import OrdersTable from "./components/OrdersTable";
import "./order.less";

class Orders extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.setOrders();
  }

  render() {
    return (
      <div className='orders'>
        {this.props.orders.length === 0 ? <NoOrderMessage/> : <OrdersTable orders={this.props.orders} />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  orders: state.order.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setOrders
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Orders);


