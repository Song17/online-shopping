import React, {Component} from 'react';

class Goods extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled: false
    };
    this.addQuantity = this.addQuantity.bind(this);
    this.updateOrderQuantity = this.updateOrderQuantity.bind(this);
    this.setDisabled = this.setDisabled.bind(this);
  }

  addQuantity() {
    const order = this.props.getOrder();
    let quantity = order.length === 0 ? 1 : order[0].quantity + 1;
    this.updateOrderQuantity(quantity);
  }

  updateOrderQuantity(quantity) {
    this.setDisabled(true);
    const user = "root";
    fetch(
      `http://localhost:8080/api/orders?user=${user}&goodsId=${this.props.goods.id}&quantity=${quantity}`,
      {
        method: 'PUT'
      })
      .then(response => this.props.refresh())
      .then(response => this.setDisabled(false))
      .catch(error => console.log(error));
  }

  setDisabled(disabled) {
    this.setState({disabled})
  }

  render() {
    const {name, price, unit, image} = this.props.goods;
    return (
      <div className='goods'>
        <img className='goods-img' src={image}/>
        <p className='goods-name'>{name}</p>
        <p className='goods-desc'>单价：{price}元/{unit}</p>
        <button type='button' className='add-goods' onClick={this.addQuantity} disabled={this.state.disabled}>+</button>
      </div>
    );
  }
}

export default Goods