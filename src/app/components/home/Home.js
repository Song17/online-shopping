import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {setGoods} from "../../actions/goodsAction";
import Goods from "./components/Goods";
import './home.less';
import {setOrders} from "../../actions/orderAction";

class Home extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='home'>
        {this.props.goods.map((goods, key) => (
          <Goods refresh={()=>this.props.setOrders()} goods={goods} getOrder={() => this.findOrdersByGoodsAndUser("root", goods.id)} key={key}></Goods>))}
      </div>
    );
  }

  componentDidMount() {
    this.props.setGoods();
    this.props.setOrders();
  }

  findOrdersByGoodsAndUser(user, goodsId) {
    return this.props.orders.filter(o => o.user === user && o.goodsId === goodsId);
  }

}

const mapStateToProps = state => ({
  goods: state.goods.goods,
  orders: state.order.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setGoods,
  setOrders
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);

