import React from 'react';
import {NavLink} from "react-router-dom";
import './header.less';

const Header = () => {
  return (
    <header>
      <nav>
        <ul className='header-nav-list'>
          <li>
            <NavLink activeClassName='clicked-link' className='nav-link' exact to='/online-shopping/'>商城</NavLink>
          </li>
          <li><NavLink activeClassName='clicked-link' className='nav-link' to='/online-shopping/create-goods'>添加商品</NavLink>
          </li>
          <li>
            <NavLink activeClassName='clicked-link' className='nav-link' to='/online-shopping/orders'>订单</NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;