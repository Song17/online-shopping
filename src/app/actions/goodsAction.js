export const setGoods = () => (dispatch) => {
  fetch('http://localhost:8080/api/goods')
    .then(response => response.json())
    .then(result => dispatch({
        type: 'SET_GOODS',
        goods: result
      }));
};