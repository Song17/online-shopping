export const setOrders = () => (dispatch) => {
  fetch('http://localhost:8080/api/orders')
    .then(response => response.json())
    .then(result => dispatch({
      type: 'SET_ORDERS',
      orders: result
    }));
};