import React, {Component} from 'react';
import {BrowserRouter as Router, NavLink} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from './components/home/Home';
import Header from "./components/header/Header";
import GoodsCreator from "./components/goods-creator/GoodsCreator";
import Orders from "./components/orders/Orders";

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Router>
          <Header/>
          <Switch>
            <Route path='/online-shopping/create-goods' component={GoodsCreator}></Route>
            <Route path='/online-shopping/orders' component={Orders}></Route>
            <Route path='/' component={Home}></Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;